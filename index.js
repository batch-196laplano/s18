function sum(a,b){
	let sumTotal = a + b;
	console.log("Display sum of " + a + " and " + b);
	console.log(sumTotal);
}
sum(5,15);


function difference(a,b){
	let differenceTotal = a - b;
	console.log("Display difference of " + a + " and " + b);
	console.log(differenceTotal);
}
difference(20,5);


function product(a,b){
	let total = a * b;
	console.log("Display product of " + a + " and " + b);
	return total;
}
let productTotal = product(50,10);
console.log(productTotal);

function quotient(a,b){
	let total = a / b;
	console.log("Display quotient of " + a + " and " + b);
	return total;
}
let quotientTotal = quotient(50,10);
console.log(quotientTotal);


function areaOfCircle(radius){
	const pi = 3.1416;
	let area = pi * (radius**2);
	console.log("The result of getting he area of circle with " + radius + " radius:");
	return area;
}
let areaTotal = areaOfCircle(15);
console.log(areaTotal);


function average(a,b,c,d){
	let total = (a+b+c+d)/4;
	console.log("The average of " + a + ", "+ b + ", "+ c + " and "+ d + ":");
	return total;
}
let averageTotal = average(20,40,60,80);
console.log(averageTotal);

function isPassing(a,b){
	let total = (a/b)*100;
	console.log("Is " + a + "/"+ b + " a passing score?");
	let isPassingScore = total >= 75;
	return isPassingScore;
}
let isPassingScore = isPassing(38,50);
console.log(isPassingScore);